package tnd.property;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Properties;

import org.apache.log4j.Logger;

public class PropertyUtil {
	
	private static final Logger logger = Logger.getLogger(PropertyUtil.class);
	
	private static void createDefaultProperty(File propertyFile,
			Class<?>[] targetClasses) {
		FileOutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream(propertyFile);
			Properties properties = new Properties();
			for (int i = 0; i < targetClasses.length; i++) {
				Class<?> targetClass = targetClasses[i];
				String prefixPropertyName = targetClass.getCanonicalName()
						+ ".";
				Field[] declaredFields = targetClass.getDeclaredFields();
				for (int j = 0; j < declaredFields.length; j++) {
					Field field = declaredFields[j];
					PropertyField annotation = field
							.getAnnotation(PropertyField.class);
					if (annotation != null) {
						try {
							field.setAccessible(true);
							Object defaultProperty = field.get(null);
							properties.setProperty(
									prefixPropertyName
											+ getFieldName(annotation,
													field.getName()),
									defaultProperty.toString());
						} catch (IllegalArgumentException e) {
							// OMG this java is bugged.
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							// should never happened.
							e.printStackTrace();
						}
					}
				}
			}
			properties.store(outputStream, "Created by tnd property utillity");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private static String getFieldName(PropertyField propertyField, String def) {
		return propertyField.value().equals("") ? def : propertyField.value();
	}

	private static void setProperty(Properties properties,
			Class<?>[] targetClasses) {
		for (int i = 0; i < targetClasses.length; i++) {
			Class<?> targetClass = targetClasses[i];
			String prefixPropertyName = targetClass.getCanonicalName() + ".";
			Field[] declaredFields = targetClass.getDeclaredFields();
			for (int j = 0; j < declaredFields.length; j++) {
				Field field = declaredFields[j];
				PropertyField annotation = field
						.getAnnotation(PropertyField.class);
				if (annotation != null) {
					String property = properties.getProperty(prefixPropertyName
							+ getFieldName(annotation, field.getName()));
					if (property != null) {
						try {
							Field modifiers = Field.class
									.getDeclaredField("modifiers");
							modifiers.setAccessible(true);
							modifiers.set(field, field.getModifiers()
									& ~Modifier.FINAL);
							field.setAccessible(true);
							field.set(null, property);
						} catch (IllegalArgumentException e) {
							// wtf property should be string.
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							// should never happened.
							e.printStackTrace();
						} catch (NoSuchFieldException e) {
							// PropertyUtil class implementation failed
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	public static void loadProperty(String propertyFile,
			Class<?>[] targetClasses) {
		InputStream inputStream = null;
		try {
			inputStream = PropertyUtil.class.getResourceAsStream("/resources/" + propertyFile);
			
			if (inputStream == null) {
				logger.warn("Can't find resource name: " + propertyFile + " will use external file instead.");
				inputStream = new FileInputStream(new File(propertyFile));
			}
			Properties properties = new Properties();
			properties.load(inputStream);
			setProperty(properties, targetClasses);
		} catch (FileNotFoundException e) {
			// no properties file create new.
			createDefaultProperty(new File(propertyFile), targetClasses);
		} catch (IOException e) {
			// failed to read properties
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
