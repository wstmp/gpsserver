package com.pccth.gps.GPSServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import tnd.property.PropertyField;

public class GPSTracker {

	private static final Logger logger = Logger.getLogger(MyRunnable.class);

	@PropertyField
	private static String INSERT_LOG_URL = "http://mobilesrv.wisesoft.co.th:8080/GPSTrackerService/location/insertlog";
	@PropertyField
	private static String JSON_MSG_KEY = "msg";
	
	public static void sendGPSLog(final String line) {
		MyExecutor.getInstance().execute(new Runnable() {
			public void run() {
				OutputStream output = null;
				BufferedReader reader = null;
				try {
					JSONObject json = new JSONObject();
					json.put(JSON_MSG_KEY, line);
					String requestData = "requestMsg=" + json.toString();
					URL url = new URL(null, INSERT_LOG_URL);
					HttpURLConnection connection = (HttpURLConnection) url
							.openConnection();
					connection.setRequestMethod("POST");
					connection.setRequestProperty("Content-Type",
							"application/x-www-form-urlencoded");
					connection.setFixedLengthStreamingMode(requestData.length()); // not use transfer-encoding chunked
					connection.setDoOutput(true);
					connection.setDoInput(true);
					connection.connect();
					output = connection.getOutputStream();
					output.write(requestData.getBytes());
					reader = new BufferedReader(new InputStreamReader(
							connection.getInputStream()));
					String line;
					while ((line = reader.readLine()) != null) {
						logger.info("server response: " + line);
					}
				} catch (JSONException e) {
					logger.error("Can't create json object", e);
					e.printStackTrace();
				} catch (ProtocolException e) {
					logger.error("Protocol Exception?", e);
					e.printStackTrace();
				} catch (IOException e) {
					logger.error("IOException while sending log to server", e);
					e.printStackTrace();
				} finally {
					if (output != null) {
						try {
							output.close();
						} catch (IOException e) {
							logger.error(
									"output http url connection not close successfully",
									e);
							e.printStackTrace();
						}
					}
					if (reader != null) {
						try {
							reader.close();
						} catch (IOException e) {
							logger.error(
									"input http url connection not close successfully",
									e);
							e.printStackTrace();
						}
					}
				}
			}
		});
	}
}
