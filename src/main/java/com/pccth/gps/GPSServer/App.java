package com.pccth.gps.GPSServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Set;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import tnd.property.PropertyField;
import tnd.property.PropertyUtil;

/**
 * Hello world!
 *
 */
public class App {
	private static final Logger logger = Logger.getLogger(App.class);
	private static final String propertiesFileName = "GPSServer.properties";
	@PropertyField
	private static String GPS_RECEIVER_PORT = "8443";

	public static void main(String args[]) {
		
		BasicConfigurator.configure();
		
		PropertyUtil.loadProperty(propertiesFileName, new Class<?>[] {
				App.class, MyExecutor.class, GPSTracker.class });

		ServerSocketChannel server = null;
		try {
			server = ServerSocketChannel.open();
			// bind to port(same as "bind" call in C on Linux platform).
			// Can wait for accepted 1000 queues(same as "listen" call in C on
			// Linux platform). Actually this is useless.
			server.socket().bind(
					new InetSocketAddress(Integer.parseInt(GPS_RECEIVER_PORT)),
					1000);
			logger.info("start server with port " + GPS_RECEIVER_PORT);
			server.configureBlocking(false);

			final Selector selector = Selector.open();
			final ServerSocketChannel _server = server;
			server.register(selector,
					SelectionKey.OP_ACCEPT, new Runnable() {
				public void run() {
					try {
						SocketChannel socket = _server.accept();
						if(socket == null) {
							return;
						}
						socket.configureBlocking(false);
						try {
							final SelectionKey socketKey = socket.register(selector,
									SelectionKey.OP_READ/*
														 * |
														 * SelectionKey.OP_WRITE
														 */);
							socketKey.attach(new MyRunnable(socketKey));
						} catch (ClosedChannelException e) {
							//this should never happen.
							logger.debug("trying to accept connection that already closed.",
									e);
						}
					} catch (IOException e) {
						logger.debug("server accept failed", e);
						e.printStackTrace();
					}
				}
			});

			while (true) {
				int fdSetCount = selector.select();
				if (fdSetCount == 0) {
					continue;
				}
				Set<SelectionKey> selectedFds = selector.selectedKeys();
				for (SelectionKey fd : selectedFds) {
					((Runnable) fd.attachment()).run();
					selectedFds.remove(fd);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (server != null) {
				try {
					server.close();
				} catch (IOException e) {
					logger.debug("Can't close server", e);
					e.printStackTrace();
				}
			}
		}
	}
}
