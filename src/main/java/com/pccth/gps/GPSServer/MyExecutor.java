package com.pccth.gps.GPSServer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import tnd.property.PropertyField;

public class MyExecutor {
	
	@PropertyField
	private static String THREAD_POOL_COUNT = "50";
	
	private static MyExecutor instance;
	public static MyExecutor getInstance(){
		if (instance == null) {
			instance = new MyExecutor();
		}
		return instance;
	}
	
	private final ExecutorService threadPool;
	
	public MyExecutor(){
		threadPool = Executors.newFixedThreadPool(Integer.parseInt(THREAD_POOL_COUNT));
	}
	
	public void execute(Runnable run){
		threadPool.execute(run);
	}
}
