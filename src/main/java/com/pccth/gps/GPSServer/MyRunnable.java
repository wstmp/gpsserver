package com.pccth.gps.GPSServer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

public class MyRunnable implements Runnable {
	
	private static final Logger logger = Logger.getLogger(MyRunnable.class);

	private static int totalRunnableId = 0;
	private int runnableId = ++totalRunnableId;
	
	private SocketChannel clientSocket;
	private String readedString = "";
	private SelectionKey socketKey;
	ByteBuffer buffer = ByteBuffer.allocate(1024);

	public MyRunnable(SelectionKey socketKey) {
		this.socketKey = socketKey;
		this.clientSocket = (SocketChannel) socketKey.channel();
		logger.info("client accepted Id: " + runnableId);
		// 1024 byte should be able to hold the streaming data;
	}

	public void run() {
		try {
			buffer.clear();
			int readed = clientSocket.read(buffer);
			if (readed == -1) {
				rejectClient("Reach EOF.");
				processLine(readedString);
			}
			readedString += new String(buffer.array(), 0, buffer.position(),
					Charset.defaultCharset());
			int LF = readedString.indexOf("\n");
			int CR = readedString.indexOf("\r");
			if (LF >= 0 || CR >= 0) {
				int nextLineIndex;
				int indexOfNewLine;
				if (LF == -1) {
					nextLineIndex = CR + 1;
					indexOfNewLine = CR;
				} else if (CR == -1) {
					nextLineIndex = LF + 1;
					indexOfNewLine = LF;
				} else {
					nextLineIndex = LF < CR ? LF + 1 : CR == LF - 1 ? LF + 1
							: CR + 1;
					indexOfNewLine = LF < CR ? LF : CR;
				}
				processLine(readedString.substring(0, indexOfNewLine));
				readedString = readedString.substring(nextLineIndex);
			}
		} catch (IOException e) {
			logger.error("IOException if some other I/O error occurs?", e);
			rejectClient("QuickFix drop the current connection.");
		}
	}

	private boolean checkIMEI(String imei) {
		if (imei.length() != 15) {
			return false;
		}
		return true;
//		int sum = 0;
//		for (int i = 0; i < 15; ++i) {
//			int numericValue = Character.getNumericValue(imei.charAt(i));
//			sum += (i % 2) == 0 ? numericValue : numericValue * 2;
//			System.out.println(sum);
//		}
//		if (sum % 10 == 0) {
//			return true;
//		} else {
//			return false;
//		}
	}

	private boolean checkIsValidDouble(String check) {
		final String Digits = "(\\p{Digit}+)";
		final String HexDigits = "(\\p{XDigit}+)";
		// an exponent is 'e' or 'E' followed by an optionally
		// signed decimal integer.
		final String Exp = "[eE][+-]?" + Digits;
		final String fpRegex = ("[\\x00-\\x20]*" + // Optional leading
													// "whitespace"
				"[+-]?(" + // Optional sign character
				"NaN|" + // "NaN" string
				"Infinity|" + // "Infinity" string

				// A decimal floating-point string representing a finite
				// positive
				// number without a leading sign has at most five basic pieces:
				// Digits . Digits ExponentPart FloatTypeSuffix
				//
				// Since this method allows integer-only strings as input
				// in addition to strings of floating-point literals, the
				// two sub-patterns below are simplifications of the grammar
				// productions from the Java Language Specification, 2nd
				// edition, section 3.10.2.

				// Digits ._opt Digits_opt ExponentPart_opt FloatTypeSuffix_opt
				"(((" + Digits + "(\\.)?(" + Digits + "?)(" + Exp + ")?)|" +

		// . Digits ExponentPart_opt FloatTypeSuffix_opt
				"(\\.(" + Digits + ")(" + Exp + ")?)|" +

				// Hexadecimal strings
				"((" +
				// 0[xX] HexDigits ._opt BinaryExponent FloatTypeSuffix_opt
				"(0[xX]" + HexDigits + "(\\.)?)|" +

				// 0[xX] HexDigits_opt . HexDigits BinaryExponent
				// FloatTypeSuffix_opt
				"(0[xX]" + HexDigits + "?(\\.)" + HexDigits + ")" +

				")[pP][+-]?" + Digits + "))" + "[fFdD]?))" + "[\\x00-\\x20]*");// Optional
																				// trailing
																				// "whitespace"

		if (Pattern.matches(fpRegex, check)){
			return true;
		} else {
			return false;
		}
	}

	private void processLine(String line) {
		logger.info("Id: " + runnableId + " processing: " + line);
		// validating
		String[] splited = line.split(",");
		if (splited.length != 21) {
			// not valid commas count reject
			rejectClient("invalid number of commas " + splited.length);
			return;
		}
		if (!checkIMEI(splited[1])) {
			// not valid International Mobile Station Equipment Identity
			rejectClient(splited[1] + " is not valid imei");
			return;
		}
		if (!checkIsValidDouble(splited[4])){
			// not valid lat
			rejectClient("latitude " + splited[4] + " is not a double");
			return;
		}
		if (!checkIsValidDouble(splited[5])){
			// not valid long
			rejectClient("longitude " + splited[5] + " is not a double");
			return;
		}
//		String imei = splited[1];
//		String lat = splited[4];
//		String lng = splited[5];
		GPSTracker.sendGPSLog(line);
	}

	private void rejectClient(String reason) {
		logger.info("Perform closing socket for client: " + runnableId + ", reason: " + reason);
		socketKey.cancel();
		try {
			clientSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
